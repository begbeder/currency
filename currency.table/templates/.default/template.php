<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<table class="table">
	<thead>
		<tr>
			<th>Название</th>
			<th>Метро</th>
			<th>Телефоны</th>
			<th>Покупка</th>
			<th>Продажа</th>
		</tr>
	</thead>
	<tbody>
		<?foreach ($arResult["EXCHANGES"] as $key => $arExchange):?>
			<tr id="exc_<?=md5($arExchange["UF_NAME"])?>">
				<td><?=$arExchange["UF_NAME"]?></td>
				<td><?=$arExchange["UF_METRO"]?></td>
				<td>
					<?foreach($arExchange["UF_PHONES"] as $phone):?>
						<?=$phone?><br>
					<?endforeach?>
				</td>
				<td>
					<?foreach ($arExchange["RATE"] as $key => $arRate):?>
						<?=$arRate["UF_SUM"]?> - <?=$arRate["UF_BUY"]?>
					<?endforeach?>
				</td>
				<td>
					<?foreach ($arExchange["RATE"] as $key => $arRate):?>
						<?=$arRate["UF_SUM"]?> - <?=$arRate["UF_SELL"]?>
					<?endforeach?>
				</td>
			</tr>
		<?endforeach?>
	</tbody>


</table>

<script type="text/javascript">
	function update_tr(tr)
	{
		$('#' + tr).addClass('updateRow');
	}


	BX.PULL.extendWatch('UpdateCurrency');
	BX.PULL.extendWatch('AddExchange');
	BX.addCustomEvent("onPullEvent", function(module_id,command,params) {
		var fixxxer = params;
		// console.log(fixxxer);
	    if (command == 'UpdateCurrency')
	    {

	    	for (var i in fixxxer)
	    	{
	    		if(i != 'SERVER_TIME')
	    		{
		    			// console.log(fixxxer[i]);
	    			$('#' + i).find('td').eq(3).text(fixxxer[i]["TEXT_BUY"])
	    			$('#' + i).find('td').eq(4).text(fixxxer[i]["TEXT_SELL"])
	    			update_tr(i);

	    		}
	    	}
	    	// // console.log(fixxxer);
	    }
	    if(command == 'AddExchange')
	    {
	    	for (var i in fixxxer)
	    	{
	    		if(i != 'SERVER_TIME')
	    		{
	    			$('.table tbody').append('<tr class="' + fixxxer[i]["TR_ID"] + '"><td>' + fixxxer[i]["UF_NAME"] + '</td> \
	    				<td>' + fixxxer[i]["UF_METRO"] + '</td><td>' + fixxxer[i]["UF_PHONES"][0] + '</td><td></td><td></td>');
	    		}
	    	}
	    }
	});
</script>