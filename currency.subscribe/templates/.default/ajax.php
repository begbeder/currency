<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?>
<? use Bitrix\Main\Loader;
   use Bitrix\Main\Application;
   use Bitrix\Main\HttpRequest as Request;
   use Bitrix\Highloadblock as HL;

if(Loader::includeModule('highloadblock'))
{
	$hlblockSubscribes     = HL\HighloadBlockTable::getById(12)->fetch();
    $entitySubscribes      = HL\HighloadBlockTable::compileEntity( $hlblockSubscribes );
    $entityClassSubscribes = $entitySubscribes->getDataClass();

    $request = Application::getInstance()->getContext()->getRequest();

    $data = array(
    	"UF_RULE"   => $request->getPost("RULE"),
    	"UF_IS_BUY" => $request->getPost("IS_BUY"),
    	"UF_EMAIL"  => $request->getPost("EMAIL"),
    	"UF_PRICE"  => $request->getPost("PRICE")
    );
    $result = $entityClassSubscribes::add($data);
    // echo json_encode($result);
}





?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>