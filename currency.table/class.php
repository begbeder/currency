<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity as Entity;
use Bitrix\Main\Loader as Loader;
use Bitrix\Main\Type\DateTime as DateTime;


class CurrencyTable extends CBitrixComponent
{


	private function prepareParams()
	{

	}
	private function prepareData()
	{
		if(Loader::includeModule('highloadblock') && Loader::includeModule('pull'))
		{		
			$hlblockExchange   = HL\HighloadBlockTable::getById(10)->fetch();
			$entityExchange   = HL\HighloadBlockTable::compileEntity( $hlblockExchange );
			$entityClassExchange = $entityExchange->getDataClass();
			$rsExchanges = new Entity\Query($entityExchange);
			$rsExchanges->setSelect(array("*"));
			$result = $rsExchanges->exec();
			$result = new CDBResult($result);
			while($arExchanges = $result->Fetch())
			{
				$this->arResult["EXCHANGES"][$arExchanges["UF_ID"]] = $arExchanges;
			}
			$hlblockRate   = HL\HighloadBlockTable::getById(11)->fetch();
			$entityRate   = HL\HighloadBlockTable::compileEntity( $hlblockRate ); 
			$entityClassRate = $entityRate->getDataClass();
			$rsRate = new Entity\Query($entityRate);
			$rsRate->setSelect(array("*"));
			$rsRate->setFilter(array("UF_ACTUAL" => 1));
			$result = $rsRate->exec();
			$result = new CDBResult($result);
			while ($arRate = $result->Fetch()) 
			{
				$this->arResult["EXCHANGES"][$arRate["UF_EXCHANGE"]]["RATE"][$arRate["UF_SUM"]] = $arRate;
			}

		}
	}

	public function executeComponent()
	{
		$this->prepareParams();
		$this->prepareData();
		$this->includeComponentTemplate();
	}
}